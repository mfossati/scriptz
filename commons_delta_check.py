#!/usr/bin/env python3
# coding: utf-8

from sys import argv, exit

from wmfdata.spark import get_session, run
from pyspark.sql import functions as F

seed = 1984

if __name__ == '__main__':
    if len(argv) != 5:
        print(f'Usage: python {__file__} previous previous_delta current current_delta')
        exit(1)

    spark = get_session(app_name='T302434')

    p = spark.read.parquet(argv[1])
    pd = spark.read.parquet(argv[2])
    c = spark.read.parquet(argv[3])
    d = spark.read.parquet(argv[4])

    print()
    print('!!! Check 1: previous full and previous delta -> same count')
    print('Full:', p.count(), '. Delta:', pd.count())

    print()
    print('!!! Check 2: join __DELETE_GROUPING__ delta values with current -> empty result')
    d.where(F.array_contains(d.values, '__DELETE_GROUPING__')).join(c, on=[d.values == c.values]).select(d.page_id, d.tag, d.values).show(truncate=False)

    print('!!! Check 3: two random pages with tags that have more than 3 values -> manual check')
    mt3 = p.where(F.size(p.values) > 3).sample(fraction=0.0001, seed=1984).select(p.page_id).take(2)
    mt3_ids = [r.page_id for r in mt3]
    print('Previous:')
    p.where(p.page_id.isin(mt3_ids)).select(p.page_id, p.tag, p.values).orderBy(p.page_id).show(truncate=False)
    print('Current:')
    c.where(c.page_id.isin(mt3_ids)).select(c.page_id, c.tag, c.values).orderBy(c.page_id).show(truncate=False)
    print('Delta:')
    d.where(d.page_id.isin(mt3_ids)).select(d.page_id, d.tag, d.values).orderBy(d.page_id).show(truncate=False)

    print('!!! Check 4:  ten random  __DELETE_GROUPING__ delta values -> manual check')
    dg = d.where(F.array_contains(d.values, '__DELETE_GROUPING__')).sample(fraction=0.0001, seed=1984).select(d.page_id).take(10)
    dg_ids = [r.page_id for r in dg]
    print('Previous:')
    p.where(p.page_id.isin(dg_ids)).select(p.page_id, p.tag, p.values).orderBy(p.page_id).show(truncate=False)
    print('Current:')
    c.where(c.page_id.isin(dg_ids)).select(c.page_id, c.tag, c.values).orderBy(c.page_id).show(truncate=False)
    print('Delta:')
    d.where(d.page_id.isin(dg_ids)).select(d.page_id, d.tag, d.values).orderBy(d.page_id).show(truncate=False)

    spark.stop()
    exit(0)

