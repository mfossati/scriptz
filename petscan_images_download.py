#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import gzip
import hashlib
import json
import os
from math import ceil
from sys import exit
from typing import Iterator, Union

import requests
from tqdm import tqdm

# Internal MediaWiki API endpoint
# https://wikitech.wikimedia.org/wiki/Data_Platform/Internal_API_requests#Using_requests
MW_API_URL = 'https://mw-api-int-ro.discovery.wmnet:4446/w/api.php'
# Public endpoint
# HOST = 'https://upload.wikimedia.org'
# Internal endpoint
# https://phabricator.wikimedia.org/T221761#5156653
HOST = 'https://ms-fe.svc.eqiad.wmnet'
PROJECT = 'commons'
TIMEOUT = 15
MIN_WIDTH = 224
DEFAULT_WIDTH = 600
# Based on https://commons.wikimedia.org/wiki/Special:MediaStatistics
ALLOWED_EXTENSIONS = (
        'bmp',
        'djv',
        'djvu',
        'gif',
        'jpg',
        'jpeg',
        'jpe',
        'jps',
        'png',
        'apng',
        'svg',
        'tif',
        'tiff',
        'webp',
        'xcf',
)
TOTAL = 12000
OUTPUT_DIR = 'petscan_images'


def collect_file_names(
    petscan: Union[str, int], allowed_extensions: list, dump: bool
) -> Union[Iterator, int]:
    # Path to PetScan result
    if isinstance(petscan, str):
        with gzip.open(petscan, 'rt') as fin:
            data = json.load(fin)
    # PetScan ID
    elif isinstance(petscan, int):
        url = f'https://petscan.wmflabs.org/?psid={petscan}&format=json'

        try:
            res = requests.get(url)
        except Exception as e:
            print(f"PetScan isn't working: {e}")
            return 1

        if not res.ok:
            print(f'Response by PetScan not OK: got HTTP {res.status_code}')
            return 2

        try:
            res_json = res.json()
        except:
            print('Got no JSON from PetScan')
            return 3

        try:
            data = res_json['*'][0]['a']['*']
        except KeyError:
            print(
                "Couldn't get the data I need, check out the raw response:",
                res_json,
                sep='\n',
            )
            return 4

        if dump:
            with gzip.open(f'{petscan}_petscan.json.gz', 'wt') as fout:
                json.dump(data, fout, ensure_ascii=False)
    else:
        raise ValueError(
            f'Invalid argument: {petscan} - '
            'must be either a path to a PetScan result or a PetScan ID'
        )

    return filter(
        lambda x: x['title'].split('.')[-1].lower() in allowed_extensions,
        data,
    )


# Same as `filenames_images_download.py`
def compute_width(page_title: str) -> int:
    headers = {'Host': 'commons.wikimedia.org'}
    params = {
        'action': 'query',
        'prop': 'pageimages',
        'titles': page_title,
        'format': 'json',
    }

    try:
        res = requests.get(MW_API_URL, headers=headers, params=params)
    except Exception as e:
        print(f"MW API isn't working: {e}")
        return DEFAULT_WIDTH

    if not res.ok:
        print(f'Response by MW API not OK: got HTTP {res.status_code}')
        return DEFAULT_WIDTH

    try:
        res_json = res.json()
    except:
        print('Got no JSON from MW API')
        return DEFAULT_WIDTH

    try:
        data = res_json['query']['pages']
        page_id = list(data.keys())[0]
        width = data[page_id]['thumbnail']['width']
        height = data[page_id]['thumbnail']['height']
    except (KeyError, IndexError):
        print(
            "Couldn't get the data I need, check out the raw response:",
            res_json,
            sep='\n',
        )
        return DEFAULT_WIDTH

    # Ported from
    # https://github.com/wikimedia/mediawiki-extensions-UploadWizard/blob/56257b80af6165fbbcee2f60e15a9ba1377a0f60/includes/ApiMediaDetection.php#L122
    aspect_ratio = width / height

    return MIN_WIDTH if aspect_ratio >= 1 else ceil(MIN_WIDTH / aspect_ratio)


# Almost identical to `filenames_images_download.py`
def gather_dataset(
    file_names: Iterator, total: int,
    project: str, output_dir: str,
    host: str = HOST, timeout: int = TIMEOUT,
) -> None:
    count = 0
    for f in tqdm(file_names, total=total):
        if count == total:
            break

        try:
            file_name = f['title']
        except KeyError:
            print(f'No title: {f}')
            continue

        file_extension = file_name.split('.')[-1].lower()

        # Build the URL
        md5 = hashlib.md5(file_name.encode('utf-8')).hexdigest()
        shard = f'{md5[0]}/{md5[:2]}'
        width = compute_width(f'File:{file_name}')
        if project == 'commons':
            url = f'{host}/wikipedia/{project}/thumb/{shard}/{file_name}/{width}px-{file_name}'
        else:
            url = f'{host}/wikipedia/{project}/{shard}/{file_name}'
        # Handle rendered thumbnails
        if file_extension in ('djvu', 'djv', 'gif', 'tiff', 'tif', 'webp'):
            url += '.jpg'
            file_name += '.jpg'
        elif file_extension in ('svg', 'xcf'):
            url += '.png'
            file_name += '.png'

        # Fire the HTTP request
        try:
            res = requests.get(url, stream=True, verify=True, timeout=timeout)
        except Exception as e:
            print(f'{file_name} - endpoint not working: {e}')
            continue

        if not res.ok:
            print(f'{file_name} - not OK: got HTTP {res.status_code}')
            continue

        # Write file
        res_bytes = b''
        for chunk in res.iter_content(1024):
            res_bytes += chunk

        with open(os.path.join(output_dir, file_name), 'wb') as fout:
            fout.write(res_bytes)

        count += 1


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description='Gather a dataset of images from a PetScan query'
    )
    # Required
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        '-i', '--petscan-id', metavar='N', type=int,
        help='PetScan ID, see https://petscan.wmflabs.org/',
    )
    group.add_argument(
        '-r', '--petscan-result', metavar='/path/to/json.gz',
        help='Gzipped JSON PetScan result, as output by "--dump-petscan"',
    )
    # Optional
    parser.add_argument(
        '-o', '--output', metavar='/path/to/dir', default=OUTPUT_DIR,
        help=f'Output directory. Default: "{OUTPUT_DIR}"',
    )
    parser.add_argument(
        '-p', '--project', metavar='wiki', default=PROJECT,
        help=f'Wiki project to gather images from. Default: {PROJECT}'
    )
    parser.add_argument(
        '-e', '--extensions', nargs='+', metavar='ext', default=ALLOWED_EXTENSIONS,
        help=f'File extensions to keep. Default: {ALLOWED_EXTENSIONS}'
    )
    parser.add_argument(
        '-d', '--dump-petscan', action=argparse.BooleanOptionalAction, default=False,
        help="Dump PetScan result to a gzipped JSON file. Default: don't"
    )
    parser.add_argument(
        '-t', '--total', type=int, metavar='N', default=TOTAL,
        help=f'How many images to download. Default: {TOTAL}'
    )

    return parser.parse_args()


def main():
    args = parse_args()
    petscan = args.petscan_id if args.petscan_id else args.petscan_result
    output = args.output
    project = args.project
    extensions = args.extensions
    dump_petscan = args.dump_petscan
    total = args.total

    os.makedirs(output, exist_ok=True)
    # https://wikitech.wikimedia.org/wiki/Data_Platform/Internal_API_requests#Python
    os.environ['REQUESTS_CA_BUNDLE'] = '/etc/ssl/certs/ca-certificates.crt'

    file_names = collect_file_names(petscan, extensions, dump_petscan)
    if type(file_names) == int:
        return file_names

    gather_dataset(file_names, total, project, output)

    return 0


if __name__ == '__main__':
    exit(main())
