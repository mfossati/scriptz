#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# mamba create -n gogo python
# mamba activate gogo
# pip install keras tensorflow keras-cv

# NOTE options that might improve performance:
#      https://keras.io/examples/vision/image_classification_efficientnet_fine_tuning/#tips-for-fine-tuning-efficientnet
#      [x] decrease batch size
#      - decrease learning rate if loss > log(NUM_CLASSES)
#
#      [x] increase training samples
#          2x -> +3% val accuracy
#      - early stopping if overfitting
#
#      https://keras.io/guides/keras_cv/classification_with_keras_cv/#optimizer-tuning
#      [x] learning rate schedule
#
#      https://keras.io/examples/vision/image_classification_efficientnet_fine_tuning/#transfer-learning-from-pretrained-weights
#      [~] increase epochs up to 50
#      [] unfreeze N layers, decrease learning rate & train again - see step 2
#
#      https://keras.io/guides/keras_cv/cut_mix_mix_up_and_rand_augment/#cutmix-and-mixup-generate-highquality-interclass-examples
#      https://keras.io/guides/keras_cv/classification_with_keras_cv/#data-augmentation
#      [x] RandomCropAndResize
#      [x] RandAugment
#      [] CutMix or MixUp with RandomChoice
#      keras_cv.layers.RandomChoice(
#          [keras_cv.layers.CutMix(), keras_cv.layers.MixUp()],
#          batchwise=True,
#      )
#
#      https://keras.io/api/optimizers/learning_rate_schedules/cosine_decay/
#      https://keras.io/guides/keras_cv/classification_with_keras_cv/#optimizer-tuning
#      [x] Learning rate schedule with cosine decay

import math
from datetime import date
from json import dump, dumps
from os import makedirs, path
from sys import argv, exit
from typing import List

import keras
import keras_cv
import tensorflow as tf
from keras import ops

CLASS_NAMES = (
    'logo', 'apportionment_diagram', 'chemical_structure', 'coat_of_arms', 'flag',
    'glyph', 'route_sign', 'safety_symbol', 'signature', 'sport_kit',
)
NUM_CLASSES = len(CLASS_NAMES)

N_THREADS = 24
SEED = 1984

# Dataset params
VALIDATION_SPLIT = 0.15
IMAGE_SIZE = (224, 224)
BATCH_SIZE = 64

# Data augmentation params
CONTRAST_FACTOR = 0.11
ROTATION_FACTOR = 0.16
TRANSLATION_FACTOR = 0.084
CROP_AREA_FACTOR = (0.8, 1.0)
ASPECT_RATIO_FACTOR = (0.9, 1.1)
# RandAugment
# https://keras.io/guides/keras_cv/cut_mix_mix_up_and_rand_augment/#randaugment
MAGNITUDE = 0.3
MAGNITUDE_STDDEV = 0.2
RATE = 1.0

# Model params
# Smallest EfficientNet variant, see
# https://keras.io/api/keras_cv/models/#backbone-presets
# NOTE Rescaling is included by default in the preset via `include_rescaling=True`,
#      so the dataset is expected to have pixels in the `[0, 255]` range.
BACKBONE_PRESET = 'efficientnetv2_b0_imagenet'
# Learning rate & epochs as per
# https://keras.io/examples/vision/image_classification_efficientnet_fine_tuning/#transfer-learning-from-pretrained-weights
LEARNING_RATE = 1e-2
EPOCHS = 25
METRICS = ['accuracy']  # 'auc_pr', 'auc_roc'


def build_augmentation_layers():
    return [
        keras.layers.RandomFlip(seed=SEED),
        keras.layers.RandomRotation(ROTATION_FACTOR, seed=SEED),
        keras_cv.layers.RandomCropAndResize(
            target_size=IMAGE_SIZE,
            crop_area_factor=CROP_AREA_FACTOR,
            aspect_ratio_factor=ASPECT_RATIO_FACTOR,
            seed=SEED,
        ),
        keras_cv.layers.RandAugment(
            value_range=(0, 255),
            magnitude=MAGNITUDE,
            magnitude_stddev=MAGNITUDE_STDDEV,
            rate=RATE,
            seed=SEED,
        ),
        # NOTE `RandAugment` can trigger `RandomContrast`, `TranslateX`, or TranslateY`:
        #      either it or the following layers.
        # keras.layers.RandomContrast(CONTRAST_FACTOR, seed=SEED),
        # keras.layers.RandomTranslation(
            # height_factor=TRANSLATION_FACTOR, width_factor=TRANSLATION_FACTOR, seed=SEED,
        # ),
    ]


def augment(image: tf.Tensor, augmentation_layers: List[keras.Layer]) -> tf.Tensor:
    for layer in augmentation_layers:
        image = layer(image)

    return image


def build_optimizer(with_cosine_decay = False, total_samples = None) -> keras.Optimizer:
    if with_cosine_decay:
        if total_samples is None:
            raise ValueError('You must provide the total amount of training samples')

        # https://keras.io/guides/keras_cv/classification_with_keras_cv/#optimizer-tuning
        # https://keras.io/api/optimizers/learning_rate_schedules/cosine_decay/
        total_steps = (total_samples // BATCH_SIZE) * EPOCHS
        warmup_steps = int(0.1 * total_steps)
        start_lr = 0.05
        decay_steps = int(0.45 * total_steps)

        from keras.optimizers.schedules import CosineDecay
        schedule = CosineDecay(
            start_lr, decay_steps, warmup_target=LEARNING_RATE,
            warmup_steps=warmup_steps
        )
        optimizer = keras.optimizers.Adam(
            weight_decay=5e-4,
            learning_rate=schedule,
        )
    else:
        optimizer = keras.optimizers.Adam(learning_rate=LEARNING_RATE)

    return optimizer


def build_metrics():
    metrics = []
    for metric in METRICS:
        if metric == 'auc_pr':
            metrics.append(
                keras.metrics.AUC(
                    curve='PR', name='auc_pr',
                    multi_label=True, num_labels=NUM_CLASSES,
                )
            )
        elif metric == 'auc_roc':
            metrics.append(
                keras.metrics.AUC(
                    name='auc_roc', multi_label=True, num_labels=NUM_CLASSES,
                )
            )
        else:
            metrics.append(metric)

    return metrics


def main(args: list) -> int:
    if len(args) != 3:
        print(f'Usage: python {__file__} DATASET_DIR OUTPUT_DIR')
        return 1

    dataset_dir = args[1]
    output_dir = args[2]

    makedirs(output_dir, exist_ok=True)

    # Limit threads
    tf.config.threading.set_intra_op_parallelism_threads(N_THREADS)
    tf.config.threading.set_inter_op_parallelism_threads(N_THREADS)

    # Build dataset
    train, val = keras.utils.image_dataset_from_directory(
        dataset_dir,
        label_mode='categorical', class_names=CLASS_NAMES,
        batch_size=BATCH_SIZE, image_size=IMAGE_SIZE,
        seed=SEED, validation_split=VALIDATION_SPLIT, subset='both',
    )
    # Uncomment this for cosine decay
    #training_samples = len(train.file_paths)

    # Augment training set
    augmentation_layers = build_augmentation_layers()
    train = train.map(
        lambda img, label: (augment(img, augmentation_layers), label),
        num_parallel_calls=tf.data.AUTOTUNE,
    )

    # Build model
    # Use a pre-trained backbone preset, see `Example` in
    # https://keras.io/api/keras_cv/models/tasks/image_classifier/
    backbone = keras_cv.models.EfficientNetV2B0Backbone.from_preset(BACKBONE_PRESET)
    model = keras_cv.models.ImageClassifier(backbone=backbone, num_classes=NUM_CLASSES)

    optimizer = build_optimizer()
    metrics = build_metrics()

    model.compile(
        optimizer=optimizer, loss='categorical_crossentropy', metrics=metrics,
    )

    # Train
    callbacks = [
        keras.callbacks.ModelCheckpoint(
            path.join(output_dir, 'epoch_{epoch:02d}_{val_accuracy:.3f}.keras')
        ),
    ]
    history = model.fit(
        train, epochs=EPOCHS, validation_data=val, callbacks=callbacks,
    )

    # Save
    with open(path.join(output_dir, 'history.json'), 'w') as fout:
        dump(history.history, fout, indent=2)
    model.save(path.join(output_dir, 'model.keras'))

    return 0


if __name__ == '__main__':
    exit(main(argv))
