#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# mamba create -n cnn python=3.11
# mamba activate cnn
# pip install tensorflow
# pip install --upgrade keras
# pip install keras-cv

# TODO batch prediction

import os
import sys

import keras
import keras_cv
import numpy as np


def main(args):
    if len(args) != 3:
        print(f'Usage: python {__file__} MODEL_PATH IMAGE_DIR')
        return 1

    model_path = args[1]
    img_dir = args[2]

    # The expected model has an EfficientNetV2B0 backbone preset.
    # See https://keras.io/api/keras_cv/models/#backbone-presets and
    # https://keras.io/api/keras_cv/models/tasks/image_classifier/
    # Paper at https://arxiv.org/pdf/2104.00298.pdf
    model = keras.models.load_model(model_path)

    # Expected input shape as per
    # https://keras.io/examples/vision/image_classification_efficientnet_fine_tuning/#keras-implementation-of-efficientnet
    # NOTE Couldn't quickly find the "official" shape in the paper
    img_size = (224, 224)

    for root, _, files in os.walk(img_dir):
        for f in files:
            img_path = os.path.join(root, f)
            # Load an image as a PIL(low) object
            img = keras.utils.load_img(img_path, target_size=img_size)

            # Convert to Numpy array
            img_array = keras.utils.img_to_array(img)
            # Add the batch dimension, expected by the model
            img_array = keras.ops.expand_dims(img_array, 0)

            pred = model(img_array)

            # ImageNet class labels at
            # https://gist.github.com/marodev/7b3ac5f63b0fc5ace84fa723e72e956d or
            # https://deeplearning.cms.waikato.ac.nz/user-guide/class-maps/IMAGENET/
            max_index = np.argmax(pred)
            max_score = pred[0][max_index]

            print(img_path, 'class:', max_index, 'score:', max_score)

    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
