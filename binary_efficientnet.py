#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# mamba create -n cnn python=3.11
# mamba activate cnn
# pip install tensorflow && pip install --upgrade keras && pip install keras-cv

from datetime import date
from json import dumps
from os import makedirs
from sys import argv, exit
from typing import List

import keras
import keras_cv
import tensorflow as tf

INPUT_DIR = '/home/mfossati/{clazz}/'
OUTPUT_DIR = '/home/mfossati/zoo/{today}/{clazz}/'

N_THREADS = 16
SEED = 1984

# Dataset params
VALIDATION_SPLIT = 0.2
IMAGE_SIZE = (224, 224)
BATCH_SIZE = 64

# Data augmentation params
CONTRAST_FACTOR = 0.11
ROTATION_FACTOR = 0.16
TRANSLATION_FACTOR = 0.084

# Model params
# Smallest EfficientNet variant, see
# https://keras.io/api/keras_cv/models/#backbone-presets
# NOTE Rescaling is included by default in the preset via `include_rescaling=True`,
#      so the dataset is expected to have pixels in the `[0, 255]` range.
BACKBONE_PRESET = 'efficientnetv2_b0_imagenet'
# Learning rate & epochs as per
# https://keras.io/examples/vision/image_classification_efficientnet_fine_tuning/#transfer-learning-from-pretrained-weights
LEARNING_RATE = 1e-2
EPOCHS = 25
METRICS = ['accuracy', 'auc', 'precision', 'recall']


def augment(image: tf.Tensor, augmentation_layers: List[keras.Layer]) -> tf.Tensor:
    for layer in augmentation_layers:
        image = layer(image)

    return image


def main(args: list) -> int:
    if len(args) != 2:
        print(f'Usage: python {__file__} CLASS')
        return 1

    clazz = args[1]
    output_dir = OUTPUT_DIR.format(today=date.today(), clazz=clazz)

    makedirs(output_dir, exist_ok=True)

    # Limit threads
    tf.config.threading.set_intra_op_parallelism_threads(N_THREADS)
    tf.config.threading.set_inter_op_parallelism_threads(N_THREADS)

    # Build dataset
    train, val = keras.utils.image_dataset_from_directory(
        INPUT_DIR.format(clazz=clazz),
        label_mode='binary', class_names=('out_of_domain', clazz),
        batch_size=BATCH_SIZE, image_size=IMAGE_SIZE,
        seed=SEED, validation_split=VALIDATION_SPLIT, subset='both',
    )

    # Augment training set
    augmentation_layers = [
        keras.layers.RandomContrast(CONTRAST_FACTOR, seed=SEED),
        keras.layers.RandomFlip(seed=SEED),
        keras.layers.RandomRotation(ROTATION_FACTOR, seed=SEED),
        keras.layers.RandomTranslation(
            height_factor=TRANSLATION_FACTOR, width_factor=TRANSLATION_FACTOR, seed=SEED,
        ),
    ]
    train = train.map(
        lambda img, label: (augment(img, augmentation_layers), label),
        num_parallel_calls=tf.data.AUTOTUNE,
    )

    # Build model
    # Use a pre-trained backbone preset, see `Example` in
    # https://keras.io/api/keras_cv/models/tasks/image_classifier/
    backbone = keras_cv.models.EfficientNetV2B0Backbone.from_preset(BACKBONE_PRESET)
    model = keras_cv.models.ImageClassifier(
        backbone=backbone, num_classes=1, activation='sigmoid',
    )

    optimizer = keras.optimizers.Adam(learning_rate=LEARNING_RATE)
    metrics = []
    for metric in METRICS:
        if metric == 'auc':
            metrics.append(keras.metrics.AUC(curve='PR', name='auc'))
        else:
            metrics.append(metric)
    model.compile(
        optimizer=optimizer, loss='binary_crossentropy', metrics=metrics,
    )

    # Train
    callbacks = [
        keras.callbacks.ModelCheckpoint(
            output_dir + 'epoch_{epoch:02d}_{val_precision:.2f}.keras'
        ),
    ]
    history = model.fit(
        train, epochs=EPOCHS, validation_data=val, callbacks=callbacks,
    )
    print(dumps(history.history, indent=2))

    model.save(output_dir + 'model.keras')

    return 0


if __name__ == '__main__':
    exit(main(argv))
