#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# mamba create -n cnn python=3.11
# mamba activate cnn
# pip install tensorflow && pip install --upgrade keras && pip install keras-cv

import json
import os
from collections import defaultdict, OrderedDict
from sys import argv, exit

import keras

BATCH_SIZE = 64
IMAGE_SIZE = (224, 224)
SEED = 1984


def main(args: list) -> int:
    if len(args) != 4:
        print(f'Usage: python {__file__} CLASS MODELS_DIR TESTSET_DIR')
        return 1

    clazz = argv[1]
    models_dir = argv[2]
    testset_dir = argv[3]

    accuracies, aucs = defaultdict(list), defaultdict(list)
    losses = defaultdict(list)
    precisions, recalls = defaultdict(list), defaultdict(list)

    testset = keras.utils.image_dataset_from_directory(
        testset_dir,
        label_mode='binary', class_names=('out_of_domain', clazz),
        batch_size=BATCH_SIZE, image_size=IMAGE_SIZE, seed=SEED,
    )

    for root, _, fnames in os.walk(models_dir):
        for fn in fnames:
            model_path = os.path.join(root, fn)
            print(model_path)

            model = keras.models.load_model(model_path)
            performance = model.evaluate(testset)
            print(dict(zip(
                ['loss', 'accuracy', 'auc', 'precision', 'recall'],
                performance,
            )))

            losses[performance[0]].append(model_path)
            accuracies[performance[1]].append(model_path)
            aucs[performance[2]].append(model_path)
            precisions[performance[3]].append(model_path)
            recalls[performance[4]].append(model_path)

    min_loss = min(losses.keys())
    max_accuracy = max(accuracies.keys())
    max_auc = max(aucs.keys())
    max_precision = max(precisions.keys())
    max_recall = max(recalls.keys())

    with open(f'{clazz}_best_models', 'w') as fout:
        fout.write(
            f"""
            Min loss: {min_loss} - {losses[min_loss]}
            Max accuracy: {max_accuracy} - {accuracies[max_accuracy]}
            Max AUC: {max_auc} - {aucs[max_auc]}
            Max precision: {max_precision} - {precisions[max_precision]}
            Max recall: {max_recall} - {recalls[max_recall]}
            """
        )
    with open(f'{clazz}_losses.json', 'w') as fout:
        json.dump(
            OrderedDict(sorted(losses.items())),
            fout, indent=2,
        )
    with open(f'{clazz}_accuracies.json', 'w') as fout:
        json.dump(
            OrderedDict(sorted(accuracies.items(), reverse=True)),
            fout, indent=2,
        )
    with open(f'{clazz}_aucs.json', 'w') as fout:
        json.dump(
            OrderedDict(sorted(aucs.items(), reverse=True)),
            fout, indent=2,
        )
    with open(f'{clazz}_precisions.json', 'w') as fout:
        json.dump(
            OrderedDict(sorted(precisions.items(), reverse=True)),
            fout, indent=2,
        )
    with open(f'{clazz}_recalls.json', 'w') as fout:
        json.dump(
            OrderedDict(sorted(recalls.items(), reverse=True)),
            fout, indent=2,
        )

    return 0


if __name__ == '__main__':
    exit(main(argv))
