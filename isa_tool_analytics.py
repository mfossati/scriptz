# coding: utf-8
import requests
api = 'https://commons.wikimedia.org/w/api.php' #?action=feedrecentchanges'
params = {'action': 'query', 'list': 'recentchanges', 'rctag': 'Isa-dev [1.0]', 'format': 'json', 'rcstart': '2022-01-15T14:56:00Z',  'rcend': 'now'}
r = requests.get(api, params=params, headers={'User-Agent': 'ISA tool analytics'})
