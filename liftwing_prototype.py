# !pip install keras==3.0.4
# !pip install keras-cv==0.8.1

import os
import requests
import json
import tempfile
import shutil
from typing import List, Dict, Any
os.environ["KERAS_BACKEND"] = "tensorflow"
import keras
from tensorflow.data import Dataset

class LogoDetectionModel:
    def __init__(self):
        self.model_path = "/content/logo_detection/model/logo_max_all.keras"
        self.model = keras.models.load_model(self.model_path)
        self.batch_size = 32
        self.image_size = (224, 224)
        self.target = "logo"
        self.chunk_size = 1024

    def validate_input_data(self, input_data: List[Dict[str, Any]]) -> None:
        """
        Validates the input data to ensure it has the required fields
        and their values are of the expected types.

        TODO: when integrating with KServe raise the `InvalidInput`
        instead of `ValueError`.
        """
        # Check maximum number of items in input data
        if len(input_data) > 50:
            raise ValueError("Input data should have less than 50 images.")

        required_keys = ["filename", "url", "target"]
        for data in input_data:
            # Check if all required keys are present
            if not all(key in data for key in required_keys):
                raise ValueError("Input data has invalid key(s). Ensure each item contains: filename, url, and target.")

            # Check if values of required keys are strings
            for key in required_keys:
                if not isinstance(data[key], str):
                    raise ValueError(f"Value for key '{key}' should be a string.")

    def download_images_to_temp_dir(self, input_data: List[Dict[str, str]], temp_dir: str) -> None:
        """
        Downloads images from URLs provided in the input data and
        saves them to a temporary directory.

        TODO: when integrating with KServe use `logging.error`
        and `raise InferenceError` instead of `print`.
        """
        for data in input_data:
            image_url = data["url"]
            image_filename = os.path.join(temp_dir, data["filename"])
            try:
                response = requests.get(image_url, stream=True)
                response.raise_for_status() # Raise an exception for HTTP errors
            except Exception as e:
                print(f"Error downloading image from {image_url}: {e}")
            response_bytes = b""
            for chunk in response.iter_content(self.chunk_size):
                response_bytes += chunk
            with open(image_filename, "wb") as f:
                f.write(response_bytes)

    def create_image_dataset(self, directory: str) -> Dataset:
        """
        Creates an image dataset based on the specified directory,
        batch_size, and image_size.
        """
        dataset = keras.utils.image_dataset_from_directory(
            directory,
            labels=None,
            label_mode=None,
            batch_size=self.batch_size,
            image_size=self.image_size,
            shuffle=False,
        )
        return dataset

    def generate_predictions(self, dataset: Dataset) -> str:
        """
        Generates predictions for the given dataset using the specified
        model.
        """
        predictions_response = []
        i = 0
        # Iterate through the dataset and make batched predictions
        # NOTE The dataset is small enough for unbatched predictions,
        #      but the model expects inputs with a batch dimension anyway.
        for batch in dataset:
            raw_predictions = self.model(batch)
            for j, raw_prediction in enumerate(raw_predictions):
                file_path = dataset.file_paths[i + j]
                file_name = os.path.basename(file_path)
                prediction = {
                    "filename": file_name,
                    "target": self.target,
                    "prediction": round(float(raw_prediction[1]), ndigits=4),
                    "out_of_domain": round(float(raw_prediction[0]), ndigits=4)
                }
                predictions_response.append(prediction)
            i += len(raw_predictions)
        predictions_json: str = json.dumps(predictions_response, ensure_ascii=False, indent=4)
        return predictions_json

if __name__ == "__main__":
    input_data = [
        {
            "filename": "Elizabeth_Drive_-_border_of_Edensor_Park_and_Bonnyrigg_Heights_in_New_South_Wales_62.jpg",
            "url": "https://phab.wmfusercontent.org/file/data/bfjukphf6khghlic2rgv/PHID-FILE-34q3qs3oe6ea5o4p22lv/Elizabeth_Drive_-_border_of_Edensor_Park_and_Bonnyrigg_Heights_in_New_South_Wales_62.jpg",
            "target": "logo"
        },
        {
            "filename": "Cambia_logo.png",
            "url": "https://phab.wmfusercontent.org/file/data/mb6wynlvf3bdfw5e443f/PHID-FILE-wc27fvtkl6yv4rjdlqzn/Cambia_logo.png",
            "target": "logo"
        },
        {
            "filename": "Blooming_bush_(14248894271).jpg",
            "url": "https://phab.wmfusercontent.org/file/data/46i23voto2a4aqwo6iyb/PHID-FILE-eldmzjv4p3vwsiwsuxya/Blooming_bush_%2814248894271%29.jpg",
            "target": "logo"
        },
        {
            "filename": "BackupVault_Logo_2019.png",
            "url": "https://phab.wmfusercontent.org/file/data/licxzubl2357mpyw5hai/PHID-FILE-kygwsboczktnzfe3u2ne/BackupVault_Logo_2019.png",
            "target": "logo"
        },
        {
            "filename": "Abv.png",
            "url": "https://phab.wmfusercontent.org/file/data/l5rkhcd3vv2kk4czp2y2/PHID-FILE-wj5balvrsa73eo35j7eg/Abv.png",
            "target": "logo"
        },
        {
            "filename": "12_rue_de_Condé_-_detail.jpg",
            "url": "https://phab.wmfusercontent.org/file/data/wxtr7be45udzyjzrojr6/PHID-FILE-tnu6mrji2smn2hpm6nhv/12_rue_de_Cond%C3%A9_-_detail.jpg",
            "target": "logo"
        }
    ]

    # Load model
    logo_predictor = LogoDetectionModel()
    # Validate input data
    logo_predictor.validate_input_data(input_data)

    # Preprocess input data
    # Create a temporary directory to store images
    temp_dir = tempfile.mkdtemp()
    # Download images from URLs and save them to a temporary directory
    logo_predictor.download_images_to_temp_dir(input_data, temp_dir)
    # Create image dataset
    dataset = logo_predictor.create_image_dataset(temp_dir)

    # Return predictions
    predictions = logo_predictor.generate_predictions(dataset)
    print(predictions)

    # Delete the temporary directory after use
    shutil.rmtree(temp_dir)
