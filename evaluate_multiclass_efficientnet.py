#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# mamba create -n cnn python=3.11
# mamba activate cnn
# pip install tensorflow && pip install --upgrade keras && pip install keras-cv

import json
import os
from collections import defaultdict, OrderedDict
from sys import argv, exit

import keras

BATCH_SIZE = 64
IMAGE_SIZE = (224, 224)
SEED = 1984


def main(args: list) -> int:
    if len(args) != 4:
        print(f'Usage: python {__file__} CLASS MODELS_DIR TESTSET_DIR')
        return 1

    clazz = argv[1]
    models_dir = argv[2]
    testset_dir = argv[3]

    accuracies, auc_prs = defaultdict(list), defaultdict(list)
    auc_rocs, losses = defaultdict(list), defaultdict(list)

    testset = keras.utils.image_dataset_from_directory(
        testset_dir,
        label_mode='categorical', class_names=('out_of_domain', clazz),
        batch_size=BATCH_SIZE, image_size=IMAGE_SIZE, seed=SEED,
    )

    for root, _, fnames in os.walk(models_dir):
        for fn in fnames:
            model_path = os.path.join(root, fn)
            print(model_path)

            model = keras.models.load_model(model_path)
            performance = model.evaluate(testset)
            print(dict(zip(
                ['loss', 'accuracy', 'auc_pr', 'auc_roc'],
                performance,
            )))

            losses[performance[0]].append(model_path)
            accuracies[performance[1]].append(model_path)
            auc_prs[performance[2]].append(model_path)
            auc_rocs[performance[3]].append(model_path)

    min_loss = min(losses.keys())
    max_accuracy = max(accuracies.keys())
    max_auc_pr = max(auc_prs.keys())
    max_auc_roc = max(auc_rocs.keys())

    with open(f'{clazz}_best_models', 'w') as fout:
        fout.write(
            f"""
            Min loss: {min_loss} - {losses[min_loss]}
            Max accuracy: {max_accuracy} - {accuracies[max_accuracy]}
            Max AUC precision/recall: {max_auc_pr} - {auc_prs[max_auc_pr]}
            Max AUC ROC: {max_auc_roc} - {auc_rocs[max_auc_roc]}
            """
        )
    with open(f'{clazz}_losses.json', 'w') as fout:
        json.dump(
            OrderedDict(sorted(losses.items())),
            fout, indent=2,
        )
    with open(f'{clazz}_accuracies.json', 'w') as fout:
        json.dump(
            OrderedDict(sorted(accuracies.items(), reverse=True)),
            fout, indent=2,
        )
    with open(f'{clazz}_auc_prs.json', 'w') as fout:
        json.dump(
            OrderedDict(sorted(auc_prs.items(), reverse=True)),
            fout, indent=2,
        )
    with open(f'{clazz}_auc_rocs.json', 'w') as fout:
        json.dump(
            OrderedDict(sorted(auc_rocs.items(), reverse=True)),
            fout, indent=2,
        )

    return 0


if __name__ == '__main__':
    exit(main(argv))
