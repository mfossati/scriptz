#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# https://phabricator.wikimedia.org/T356954

from wmfdata.mariadb import run


# enwiki counts
q0 = """SELECT img_media_type, COUNT(DISTINCT(gil_to))
FROM globalimagelinks
INNER JOIN image ON img_name = gil_to
WHERE gil_page_namespace_id = 0 AND gil_wiki = 'enwiki'
GROUP BY img_media_type
"""
r0 = run(q0, 'commonswiki')
r0.to_csv('enwiki_counts.csv', header=['media_type', 'total'], index=False)

# Grand total
q1 = """SELECT img_media_type, COUNT(DISTINCT(gil_to))
FROM globalimagelinks
INNER JOIN image ON img_name = gil_to
WHERE gil_page_namespace_id = 0
GROUP BY img_media_type
"""
r1 = run(q1, 'commonswiki')
r1.to_csv('total_counts.csv', header=['media_type', 'total'], index=False)

# Per-wiki, all wikis
q2 = """SELECT img_media_type, gil_wiki, COUNT(DISTINCT(gil_to))
FROM globalimagelinks
INNER JOIN image ON img_name = gil_to
WHERE gil_page_namespace_id = 0
GROUP BY gil_wiki, img_media_type
ORDER BY gil_wiki
"""
r2 = run(q2, 'commonswiki')
r2.to_csv('per_wiki_counts.csv', header=['media_type', 'wiki', 'total'], index=False)
