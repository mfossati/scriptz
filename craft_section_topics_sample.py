#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# TODO popularity based on page views
# TODO ? articles every wikipedia should have: https://meta.wikimedia.org/wiki/List_of_articles_every_Wikipedia_should_have

# Sample by representative article types

from wmfdata.spark import create_session
from pyspark.sql import functions as F
from pandas import concat, ExcelWriter

RANDOM_SEED = 1984
TARGET_WIKIS = ('enwiki', 'arwiki', 'bnwiki', 'cswiki', 'eswiki', 'idwiki', 'ptwiki', 'ruwiki')

spark = create_session(app_name='section-topics-sample', type='yarn-large', ship_python_env=True, extra_settings={'spark.sql.shuffle.partitions': 2048, 'spark.reducer.maxReqsInFlight': 1, 'spark.shuffle.io.retryWait': '60s'})

ddf = spark.read.parquet('eval/article_score_all')
isaac = spark.read.table('isaacj.article_topics_outlinks_2021_07')

# Rank the most represented article types, confident predictions only
confident = isaac.where(isaac.score > 0.9)
# Count by QID
# type_rank = confident.groupBy(confident.topic).agg(F.countDistinct(confident.qid).alias('count')).orderBy('count', ascending=False).toPandas()
# Count by page
type_rank = confident.groupBy(confident.topic).count().orderBy('count', ascending=False).toPandas()

# Get rid of sports articles: too many tables inside
type_rank = type_rank[type_rank.topic != 'Culture.Sports']

# Sample 20 types, weight by amount of pages
stopics = type_rank.sample(n=20, weights='count', random_state=RANDOM_SEED)
# Get QIDs of sampled types with the max score
sqids = confident.where("qid != ''").where(confident.topic.isin(stopics.topic.to_list())).groupBy('wiki_db', 'qid').agg(F.max('score').alias('max_score')).orderBy('wiki_db', 'qid').withColumnRenamed('qid', 'page_qid')

# Get sampled QIDs from the section topics dataset
sbytopic = ddf.join(sqids, on=['wiki_db', 'page_qid'])

# clean = sbytopic.where(sbytopic.wiki_db.isin(target_wikis)).drop('snapshot', 'page_namespace', 'page_id', 'tf_num', 'tf_den', 'tf', 'idf_num', 'idf_den', 'idf', 'max_score')
# clean = targets.withColumnRenamed('tf_idf', 'section_score')
# clean = targets.drop_duplicates()
# Keep target wikis only and relevant columns
clean = sbytopic.drop_duplicates().drop('snapshot', 'page_namespace', 'page_id', 'max_score')

samples = []

# with ExcelWriter('/home/mfossati/samples/extra_page_type_weighted_sample.xlsx') as ew:
for target in TARGET_WIKIS:
    lang = f"{target.rstrip('wiki')}"
    # es = targets.where(targets.wiki_db == 'eswiki')
    # esqids = es.select('page_qid').distinct()

    ds = clean.where(clean.wiki_db == target).drop('wiki_db')
    # All distinct page QIDs
    qids = ds.select('page_qid').distinct().toPandas()

    # essqids = esqids.sample(0.0001)
    # essqidsdf = essqids.toPandas()
    # Sample 100 pages
    qids_sample = qids.sample(n=100, random_state=RANDOM_SEED)

    # esfinal = es.where(es.page_qid.isin(essqidsdf.page_qid.to_list()))
    # esfinaldf = esfinal.orderBy('page_qid').toPandas()
    # esfinaldf.to_csv('/home/mfossati/samples/eswiki_page_type_based_first_trial.csv', index=False)

    # Pull the sampled QIDs out
    sample = ds.where(ds.page_qid.isin(qids_sample.page_qid.to_list())).orderBy(['page_qid', 'section_index', 'topic_score'], ascending=[True, True, False]).toPandas()

    # 100 pages is too much: take 20 random section titles instead
    sections = sample['section_title'].drop_duplicates().sample(n=20, random_state=RANDOM_SEED)
    sample = sample[sample['section_title'].isin(sections)]
    samples.append(sample)

    # sample['page_qid'] = sample['page_qid'].str.replace(r'.+', lambda m: f'=HYPERLINK("https://www.wikidata.org/wiki/{m.group()}", "{m.group()}")')
    # sample['topic_qid'] = sample['topic_qid'].str.replace(r'.+', lambda m: f'=HYPERLINK("https://www.wikidata.org/wiki/{m.group()}", "{m.group()}")')
    # sample['revision_id'] = sample['revision_id'].astype(str).str.replace(r'.+', lambda m: f'=HYPERLINK("https://{lang}.wikipedia.org/w/index.php?oldid={m.group()}", "{m.group()}")')
    # sample['page_title'] = sample['page_title'].str.replace(r'.+', lambda m: f'=HYPERLINK("https://{lang}.wikipedia.org/wiki/{m.group()}", "{m.group()}")')
    # sample['topic_title'] = sample['topic_title'].str.replace(r'.+', lambda m: f'=HYPERLINK("https://{lang}.wikipedia.org/wiki/{m.group()}", "{m.group()}")')
    # sample.to_excel(ew, sheet_name=target, index=False, float_format="%.3f")

concat(samples).to_csv('/home/mfossati/samples/slis_section_topics_sample.csv', index=False)

spark.stop()
