#!/usr/bin/env bash

set -e

hive -e "SHOW PARTITIONS wmf.mediawiki_wikitext_current PARTITION(snapshot='2023-06');" | cut -d '=' -f3 > datalake_wikis.txt
