#!/usr/bin/env bash

set -e

cd ~/datapipelines

black -l 127 -S -C image-suggestions/pyspark/src/cassandra.py
isort --multi-line=5 --line-length=127 image-suggestions/pyspark/{src,tests}
autoflake --in-place --recursive --remove-all-unused-imports --remove-unused-variables image-suggestions/pyspark/{src,tests}
