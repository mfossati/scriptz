#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import json
import re
from datetime import datetime, timedelta

from wmfdata.spark import create_session
from pyspark.sql import functions as F

COMMONS_NS4_QUERY = """SELECT page_id, revision_id, page_title, revision_text
FROM wmf.mediawiki_wikitext_current
WHERE page_namespace=4 and wiki_db='commonswiki' and snapshot='{}'
"""
TIMESTAMP_RAW_RE = r'([012][0-9]:[0-5][0-9], \d{,2} (January|February|March|April|May|June|July|August|September|October|November|December) \d{4})'
DELETED_RAW_RE = r"('{3}?[Dd]eleted:?'{3}?\s?|\{\{[Dd]eletionFooter\|[Dd][Ee][Ll][Ee][Tt][Ee][Dd]\|?)"
START_RE = re.compile(TIMESTAMP_RAW_RE)
END_RE = re.compile(DELETED_RAW_RE + r'(.*)' + TIMESTAMP_RAW_RE)
OPENING_REASON_RE = re.compile(r'=?\n(.*?)(' + TIMESTAMP_RAW_RE + '|\n\*\s+\[\[\:File)')
FILEPAGE_WIKITEXT_RE = re.compile(r'\[\[:File:([^]]*)\]\]')
DR_RE = re.compile(r'\{\{delh\}\}(.*?)\{\{delf\}\}', re.DOTALL)
OUTPUT = '/home/mfossati/commons_deletions/deletion_times.jsonl'


def day_by_day(start, end):
    while end > start:
        start += timedelta(days=1)

        yield start.strftime('%Y/%m/%d')


def generate_dataset(start_date, end_date, commons_ns4, output_path):
    skipped_archives, skipped_files = 0, 0
    no_requests, no_deletions = 0, 0
    no_opening_reasons, no_closing_reasons = 0, 0
    written = 0

    with open(output_path, 'w', buffering=1) as fout:
        for dt in day_by_day(start_date, end_date):
            print(dt)

            try:
                archive = (
                    commons_ns4
                    .where(commons_ns4.page_title == f'Commons:Deletion requests/Archive/{dt}')
                    .select('revision_text')
                    .collect()[0].revision_text
                )
            except Exception as e:
                print(f'Skipping archive "Commons:Deletion requests/Archive/{dt}": {e}')
                skipped_archives += 1
                continue

            dr_page_titles = archive.split('\n')[1:]
            for dr_page_titles_with_braces in filter(None, dr_page_titles):
                dr_page_title = dr_page_titles_with_braces.strip('{}')
                print(dr_page_title)
                try:
                    wikitext = (
                        commons_ns4
                        .where(commons_ns4.page_title == dr_page_title)
                        .select('revision_text')
                        .collect()
                    )
                except Exception as e:
                    print(f'Skipping "{dr_page_title}": {e}')
                    skipped_files += 1
                    continue

                if not wikitext:
                    print(f'Skipping "{dr_page_title}": no wikitext')
                    skipped_files += 1
                    continue

                wikitext = wikitext[0].revision_text

                if re.search(DR_RE, wikitext):
                    drs = re.findall(DR_RE, wikitext)
                else:
                    drs = [wikitext]

                for index, dr in enumerate(drs):
                    start = re.search(START_RE, dr)
                    if start:
                        # we guess the first timestamp string found in the DR is when it was created
                        requested = start.group(1)
                    else:
                        # if there is no recognisable time in the DR then default to the date from the archive page
                        requested = datetime.strptime(dt, '%Y/%m/%d').strftime('%H:%M, %-d %B %Y')

                    closing_reason, deleted = None, None
                    end = re.search(END_RE, dr)
                    if end:
                        try:
                            closing_reason = end.group(2)
                        except IndexError:
                            print(f'Could not extract the closing reason from DR #{index} in "{dr_page_title}"')
                            no_closing_reasons += 1
                        try:
                            deleted = end.group(3)
                        except IndexError:
                            print(f'Could not extract the closing date from DR #{index} in "{dr_page_title}"')
                            no_deletions += 1
                    else:
                        no_closing_reasons += 1
                        no_deletions += 1

                    opening_reason = None
                    reason = re.search(OPENING_REASON_RE, dr)
                    if reason:
                        opening_reason = reason.group(1)
                    else:
                        no_opening_reasons += 1

                    filenames = re.findall(FILEPAGE_WIKITEXT_RE, dr)
                    if (len(filenames) == 0):
                        filenames = [ f"{dr_page_title.split(':')[-1]}" ]

                    for filename in filenames:
                        fout.write(
                            json.dumps(
                                {
                                    'deletion_request': dr_page_title,
                                    'page_title': filename,
                                    'requested': requested,
                                    'deleted': deleted,
                                    'opening_reason': opening_reason,
                                    'closing_reason': closing_reason,
                                },
                                ensure_ascii=False
                            ) + '\n'
                        )
                        written += 1

    print(
        f"""
        Skipped archive pages: {skipped_archives}
        Skipped files: {skipped_files}
        No request date found: {no_requests}
        No deletion date found: {no_deletions}
        No opening reason found: {no_opening_reasons}
        No closing reason found: {no_closing_reasons}
        Records written: {written}
        """
    )


def parse_args():
    parser = argparse.ArgumentParser(
        description=(
            'Generate a JSON Lines dataset of Commons deletion requests '
            'with request and deletion time stamps'
        )
    )
    parser.add_argument('snapshot', metavar='YYYY-MM', help='"wmf.mediawiki_wikitext_current" snapshot')
    parser.add_argument('start', metavar='YYYY-MM-DD', help='start from this date')
    parser.add_argument('end', metavar='YYYY-MM-DD', help='until this date')
    parser.add_argument(
        '-o', '--output', metavar='/hdfs_path/to/jsonl', default=OUTPUT,
        help=f'path to output JSON Lines. Default: "{OUTPUT}"'
    )

    return parser.parse_args()


def main():
    args = parse_args()

    spark = create_session(type='yarn-large', app_name='commons-deletions', ship_python_env=True)
    ns4 = spark.sql(COMMONS_NS4_QUERY.format(args.snapshot))
    ns4.cache()

    start = datetime.strptime(args.start, '%Y-%m-%d')
    end = datetime.strptime(args.end, '%Y-%m-%d')

    generate_dataset(start, end, ns4, args.output)

    spark.stop()


if __name__ == '__main__':
    main()

