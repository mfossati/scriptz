#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

from pyspark.sql import functions as F
from wmfdata.spark import create_session


IS_HIVE_TABLE = 'analytics_platform_eng.image_suggestions_suggestions'
ST_PARQUET = '/user/analytics-platform-eng/structured-data/section_topics'

ALIS_OUT = 'alis_dump'
SLIS_OUT = 'slis_dump'
ST_OUT = 'st_dump'


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description=(
            'Dump image suggestions and section topics production datasets. '
            'Inputs: '
            f'"{IS_HIVE_TABLE}" Hive table; '
            f'"{ST_PARQUET}" HDFS parquet. '
            'Output 3 directories to the current user HDFS home with per-wiki gzipped CSVs: '
            f'"{ALIS_OUT}", "{SLIS_OUT}", and "{ST_OUT}"'
        )
    )
    parser.add_argument('snapshot', metavar='YYYY-MM-DD', help='snapshot date')

    return parser.parse_args()


def main():
    args = parse_args()
    snapshot = args.snapshot

    spark = create_session(app_name='dump-image-suggestions')

    image_suggestions = (
        spark.read.table(IS_HIVE_TABLE)
        .where(f"snapshot='{snapshot}'")
    )
    image_suggestions = (
        image_suggestions
        .select(
            'wiki',
            'page_id', F.col('page_rev').alias('page_revision'), F.col('page_qid').alias('page_wikidata_item'),
            'section_index', F.col('section_heading').alias('section_title'),
            F.col('image').alias('suggested_image'), F.col('confidence').alias('confidence_score'),
            # Arrays become semicolon-separated strings.
            # Remove 'istype-' prefix from the `kind` column.
            F.regexp_replace(F.array_join('kind', ';'), 'istype-', '').alias('suggestion_types'),
            F.array_join('found_on', ';').alias('suggestion_sources'),
        )
        .orderBy('wiki', 'page_id')
    )
    alis = (
        image_suggestions
        .where(image_suggestions.section_index.isNull())
        .drop('section_index', 'section_title')
    )
    slis = image_suggestions.where(image_suggestions.section_index.isNotNull())

    section_topics = spark.read.parquet(f'{ST_PARQUET}/{snapshot}')
    st = (
        section_topics
        .select(
            F.col('wiki_db').alias('wiki'), 'page_id', F.col('revision_id').alias('page_revision'),
            F.col('page_qid').alias('page_wikidata_item'), 'page_title',
            'section_index', 'section_title',
            F.col('topic_qid').alias('topic_wikidata_item'), 'topic_title', 'topic_score'
        )
        .orderBy('wiki', 'page_id')
    )

    alis.write.partitionBy('wiki').csv(ALIS_OUT, compression='gzip', header=True)
    slis.write.partitionBy('wiki').csv(SLIS_OUT, compression='gzip', header=True)
    st.write.partitionBy('wiki').csv(ST_OUT, compression='gzip', header=True)

    spark.stop()


if __name__ == '__main__':
    main()
