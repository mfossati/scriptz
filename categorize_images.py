#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# mamba create -n cnn python=3.11
# mamba activate cnn
# pip install nltk tensorflow && pip install --upgrade keras && pip install keras-cv

import json
import sys
from collections import defaultdict

import keras
import keras_cv
import nltk
import numpy as np
from nltk.corpus import wordnet

BATCH_SIZE = 64
IMAGE_SIZE = (224, 224)
SEED = 1984

# Best-performing model, see
# https://keras.io/api/keras_cv/models/tasks/image_classifier/#frompreset-method
PRESET = 'efficientnetv2_s_imagenet_classifier'

# See https://observablehq.com/@mbostock/imagenet-hierarchy
# TODO refine & pick non-living entities e.g., artifact, device?
TARGET_SYNSETS = {
    'animal': ('n', 15388),     # this for sure
    'artifact': ('n', 21939),   # maybe too generic
    'fungus': ('n', 12992868),  # 7 leaf synsets
    'plant': ('n', 17222),      # 2 leaf synsets
    'person': ('n', 7846),      # 3 leaf synsets
    'fruit': ('n', 13134947),
    'beverage': ('n', 7881800),
    'geological formation': ('n', 9287968),
    'nutriment': ('n', 7570720),
    'vegetable': ('n', 7707451),
}


def build_synset_trees(class_mapping_path: str) -> dict:
    label_data = {}

    # Princeton WordNet 3.0, see https://wordnet.princeton.edu/
    nltk.download('wordnet')
    with open(class_mapping_path) as fin:
        mapping = [l.rstrip() for l in fin]

    for label, clazz in enumerate(mapping):
        wordnet_id, lemmas = clazz.split(maxsplit=1)
        pos = wordnet_id[0]
        offset = int(wordnet_id[1:].lstrip('0'))
        lemmas = [l.lstrip() for l in lemmas.split(',')]
        synset = wordnet.synset_from_pos_and_offset(pos, offset)
        trees = synset.hypernym_paths()
        label_data[label] = {
            'wordnet_id': wordnet_id,
            'lemmas': lemmas,
            'trees': trees,
        }

    return label_data


def count_target_synsets(
    raw_predictions: list, label_data: dict,
    target_synsets: dict = TARGET_SYNSETS,
) -> dict:
    output = defaultdict(int)

    for rp in raw_predictions:
        best_label = np.argmax(rp)
        score = rp[best_label]
        data = label_data[best_label]
        target_found = False

        for lemma, (pos, offset) in target_synsets.items():
            target_synset = wordnet.synset_from_pos_and_offset(pos, offset)

            for synset_tree in data['trees']:
                if target_synset in synset_tree:
                    output[lemma] += 1
                    target_found = True
                    break  # Don't look at other trees

            if target_found:
                break  # Don't look at other targets

    return output


def main(args):
    if len(args) != 3:
        print(f'Usage: python {__file__} CLASS_MAPPING_PATH IMAGE_DIR')
        return 1

    class_mapping_path = args[1]
    image_dir = args[2]

    label_data = build_synset_trees(class_mapping_path)

    # Build dataset
    dataset = keras.utils.image_dataset_from_directory(
        image_dir,
        labels=None, label_mode=None,
        batch_size=BATCH_SIZE, image_size=IMAGE_SIZE, seed=SEED,
    )

    # Predict
    model = keras_cv.models.ImageClassifier.from_preset(PRESET)
    raw_predictions = model.predict(dataset)

    counts = count_target_synsets(raw_predictions, label_data)
    print(counts)

    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
