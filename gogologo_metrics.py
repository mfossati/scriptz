#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# pip install beautifulsoup4 pandas requests

import os
import re
from urllib.parse import unquote

import pandas as pd
import requests
from bs4 import BeautifulSoup

data, deleted_urls = [], []
miss, deleted, own, templated, categorized, dr = 0, 0, 0, 0, 0, 0

df = pd.read_csv('/home/mfossati/gogologo/logos_22_days.csv')

os.environ['REQUESTS_CA_BUNDLE'] = '/etc/ssl/certs/ca-certificates.crt'

host = 'https://mw-api-int-ro.discovery.wmnet:4446'
mw_api_url = f'{host}/w/api.php'
headers = {'Host': 'commons.wikimedia.org'}
params = {
    'action': 'parse',
    'prop': 'wikitext',
    'format': 'json',
}

for row in df.itertuples():
    obj = {
        'title': row.title, 'confidence_score': row.confidence_score, 'url': row.url,
        'uploader': row.uploader, 'is_bot': row.is_bot,
        'is_own_work': False, 'category': None, 'deletion_request': None,
    }

    print(row.url)

    try:
        params['page'] = f'File:{row.title}'
        r = requests.get(mw_api_url, headers=headers, params=params).json()
        wt = r['parse']['wikitext']['*']
    except KeyError as k:
        try:
            error = r['error']['code']
        except KeyError as ke:
            print('The wikitext request went wrong, skipping: ', ke)
            miss += 1
            continue
        if error == 'missingtitle':
            print('The image was deleted')
            deleted_urls.append(f'{host}/wiki/File:{row.title}')
            deleted += 1
            continue
        else:
            print('Unexpected error:', error)
            miss += 1
            continue
    except Exception as e:
        print('The wikitext request went wrong, skipping: ', e)
        miss += 1
        continue

    obj['wikitext'] = wt

    # Detect own work
    own_work_template = '{{own}}'
    if wt.find(own_work_template) != -1:
        obj['is_own_work'] = True
        own += 1

    logo_template = '{{PD-textlogo}}'
    category_match = re.search(r'\[\[Category:.*[Ll]ogo.*\]\]', wt)
    # Detect logo template, which adds a category
    if wt.find(logo_template) != -1:
        obj['category'] = logo_template
        templated += 1
    # Detect logo category
    elif category_match is not None:
        obj['category'] = category_match.group()
        categorized += 1

    # Detect deletion request
    # Speedy deletion templates as per
    # https://commons.wikimedia.org/wiki/Category:Candidates_for_speedy_deletion
    deletion_match = re.search(
        r'\{\{(delete|copyvio|duplicate|rename|sd|speedydelete|logo)\|',
        wt, flags=re.IGNORECASE
    )
    if deletion_match is not None:
        obj['deletion_request'] = deletion_match.group(1)
        dr += 1

    data.append(obj)

output = f"""Input images = {df.shape[0]}
Missed = {miss}
Deleted = {deleted}
Own work = {own}
{logo_template} = {templated}
Categorized = {categorized}
Deletion requests = {dr}
Gain = ( (input - missed) - (deleted + templated + categorized) ) / (input - missed) * 100
     = {((df.shape[0] - missed) - (deleted + templated + categorized)) / (df.shape[0] - missed) * 100}
"""

print(output)

available = pd.DataFrame.from_records(data)
available.to_csv('/home/mfossati/gogologo/augmented_logos_22_days.csv', index=False)

# Gather information for deleted files
print()
print('Processing deleted files ...')
print()

data = []
miss = 0
for url in deleted_urls:
    o = {'url': url, 'deleters': None, 'reasons': None, 'links': None, 'taggers': None}
    print(url)

    try:
        r = requests.get(url, headers={'Host': 'commons.wikimedia.org'})
        soup = BeautifulSoup(r.text, 'html.parser')
    except Exception as e:
        print('Skipping due to :', e)
        miss += 1
        continue

    deleters = soup.select('.mw-userlink')
    if deleters:
        if len(deleters) > 1:
            print('More than one deleter:', deleters)
        o['deleters'] = '; '.join([d.get_text() for d in deleters])
    else:
        print('No deleter')

    reasons = soup.select('.comment')
    if reasons:
        if len(reasons) > 1:
            print('More than one reason:', reasons)
        o['reasons'] = '; '.join([rea.get_text() for rea in reasons])

        links = []
        for rea in reasons:
            links.extend([a.get("href") for a in rea.find_all('a')])
        o['links'] = links

        # Process deletion requests
        if links:
            taggers = []
            drs = filter(lambda x: x.find('Commons:Deletion_requests') != -1, links)
            for dr in drs:
                print(dr)
                try:
                    r = requests.get(
                        mw_api_url,
                        headers={'Host': 'commons.wikimedia.org'},
                        params={'action': 'parse', 'prop': 'wikitext', 'format': 'json', 'page': unquote(dr)},
                    )
                    wt = r.json()['parse']['wikitext']['*']
                except Exception as e:
                    print(e)
                    print(r.json())
                    continue

                m = re.search(r'\[\[User:([^\|]+)\|', wt)
                if m is not None:
                    taggers.append(m.group(1))
                else:
                    print('No DR tagger')

            if taggers:
                o['taggers'] = taggers
    else:
        print('No reasons')

    data.append(o)

already_deleted = pd.DataFrame.from_records(data)
already_deleted['reasons'] = already_deleted.reasons.str.strip('()')
already_deleted = (
    already_deleted
    .merge(available, on='url')
    .drop_duplicates()
    .filter([
        'url', 'confidence_score', 'uploader', 'is_bot',
        'deleters', 'taggers', 'reasons', 'links'
    ])
)
already_deleted.to_csv('/home/mfossati/gogologo/deleted_logos.csv', index=False)
