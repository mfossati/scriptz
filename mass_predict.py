#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import keras
import pandas as pd

INPUT_DIR = '/home/mfossati/gogologo/past_images/'


def strip_ext(f):
    ext = f.split('.')[-2].lower()
    if ext in ('djvu', 'djv', 'gif', 'tiff', 'tif', 'webp'):
        return f.split('.jpg')[0]
    elif ext in ('svg', 'xcf'):
        return f.split('.png')[0]
    else:
        return f


def main()
    ds = keras.utils.image_dataset_from_directory(INPUT_DIR, labels=None, batch_size=64, image_size=(224, 224), shuffle=False)

    base = keras.models.load_model('/home/mfossati/zoo/10k_ten_classes/model.keras')
    curated = keras.models.load_model('/home/mfossati/zoo/10k_curated_augmented_ten_classes/model.keras')
    big_curated = keras.models.load_model('/home/mfossati/zoo/15k_curated_augmented_ten_classes/model.keras')

    base_preds = pd.DataFrame.from_records(base.predict(ds), columns=classes)
    curated_preds = pd.DataFrame.from_records(curated.predict(ds), columns=classes)
    big_curated_preds = pd.DataFrame.from_records(big_curated.predict(ds), columns=classes)

    preds = pd.DataFrame()
    preds['base'] = base_preds.idxmax(axis=1)
    preds['curated'] = curated_preds.idxmax(axis=1)
    preds['big_curated'] = big_curated_preds.idxmax(axis=1)
    preds['image'] = ds.file_paths
    preds['image'] = preds['image'].str.removeprefix(INPUT_DIR)
    preds.filter(['image', 'base', 'curated', 'big_curated']).to_csv('/home/mfossati/gogologo/past_images_predictions.csv', index=False)

    logos = preds[(preds.base == 'logo') | (preds.curated == 'logo') | (preds.big_curated == 'logo')]
    logos['image'] = logos.image.apply(lambda x: f'https://commons.wikimedia.org/wiki/File:{strip_ext(x)}')
    logos = logos.set_index('image')
    logos.sample(frac=1, random_state=1984).to_csv('/home/mfossati/gogologo/past_images_logos.csv')


if __name__ == '__main__':
    main()
