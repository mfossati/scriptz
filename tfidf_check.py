#!/usr/bin/env python3
# coding: utf-8

from sys import argv, exit

from wmfdata.spark import get_session

LANGS = ('es', 'fr', 'it', 'pt', 'en')

if len(argv) != 2:
    print(f'Usage: python {__file__} SECTION_TOPICS_PARQUET')
    exit(1)

spark = get_session(app_name='section-topics-tfidf-check', type='yarn-large', extra_settings={'spark.sql.shuffle.partitions': 2048, 'spark.reducer.maxReqsInFlight': 1, 'spark.shuffle.io.retryWait': '60s'})
ddf = spark.read.parquet(argv[1])

for l in LANGS:
    print('Language:', l)
    lddf = ddf.where(ddf.wiki_db == f'{l}wiki').select('revision_id', 'page_title', 'section_id', 'section_title', 'topic_title', 'tf-idf')
    lddf.cache()
    ids = [str(row.revision_id) for row in lddf.select(lddf.revision_id).sample(0.00001).take(10)]
    sample = lddf.where(f"revision_id in ({', '.join(ids)})")
    for rev in ids:
        sample.where(sample.revision_id == int(rev)).orderBy('tf-idf', ascending=False).show(n=10, truncate=29)
spark.stop()
