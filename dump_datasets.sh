#!/usr/env/bin bash

set -e

if [[ $# -ne 2 ]]
then
    echo "Usage: bash $0 YYYY-MM-DD /path/to/output_directory"
    exit 1
fi

SNAPSHOT=$1
NO_DASH_SNAPSHOT=$(echo $SNAPSHOT | tr -d '-')
BASE_DIR_OUT=$2

python dump_datasets.py $SNAPSHOT

for dataset in {alis,slis,st}
do
    din="$dataset"_dump
    inputs=$(hdfs dfs -ls $din | tr -s ' ' | cut -s -d ' ' -f 8)
    case $dataset in
        alis) dout=article_level_image_suggestions;;
        slis) dout=section_level_image_suggestions;;
        st) dout=section_topics;;
    esac
    path_out="$BASE_DIR_OUT/$dout/$NO_DASH_SNAPSHOT"

    mkdir -p $path_out

    for fin in $inputs
    do
        wiki=$(echo $fin | cut -d '=' -f2)
        echo $fin $wiki
        hdfs dfs -getmerge $fin "$path_out/$wiki.csv.gz"
    done

    hdfs dfs -rm -r $din
    rm -r "$path_out/$din"
    chmod -R o+r $path_out
done
