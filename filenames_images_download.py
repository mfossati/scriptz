#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import hashlib
import os
from math import ceil
from sys import exit
from typing import Iterator

import pandas as pd
import requests
from tqdm import tqdm

# Internal MediaWiki API endpoint
# https://wikitech.wikimedia.org/wiki/Data_Platform/Internal_API_requests#Using_requests
MW_API_URL = 'https://mw-api-int-ro.discovery.wmnet:4446/w/api.php'
# Public endpoint
# HOST = 'https://upload.wikimedia.org'
# Internal endpoint
# https://phabricator.wikimedia.org/T221761#5156653
HOST = 'https://ms-fe.svc.eqiad.wmnet'
PROJECT = 'commons'
TIMEOUT = 15
MIN_WIDTH = 224
DEFAULT_WIDTH = 600
TOTAL = 20000
OUTPUT_DIR = 'images'


# Same as `petscan_images_download.py`
def compute_width(page_title: str) -> int:
    headers = {'Host': 'commons.wikimedia.org'}
    params = {
        'action': 'query',
        'prop': 'pageimages',
        'titles': page_title,
        'format': 'json',
    }

    try:
        res = requests.get(MW_API_URL, headers=headers, params=params)
    except Exception as e:
        print(f"MW API isn't working: {e}")
        return DEFAULT_WIDTH

    if not res.ok:
        print(f'Response by MW API not OK: got HTTP {res.status_code}')
        return DEFAULT_WIDTH

    try:
        res_json = res.json()
    except:
        print('Got no JSON from MW API')
        return DEFAULT_WIDTH

    try:
        data = res_json['query']['pages']
        page_id = list(data.keys())[0]
        width = data[page_id]['thumbnail']['width']
        height = data[page_id]['thumbnail']['height']
    except (KeyError, IndexError):
        print(
            "Couldn't get the data I need, check out the raw response:",
            res_json,
            sep='\n',
        )
        return DEFAULT_WIDTH

    # Ported from
    # https://github.com/wikimedia/mediawiki-extensions-UploadWizard/blob/56257b80af6165fbbcee2f60e15a9ba1377a0f60/includes/ApiMediaDetection.php#L122
    aspect_ratio = width / height

    return MIN_WIDTH if aspect_ratio >= 1 else ceil(MIN_WIDTH / aspect_ratio)


# Almost identical to `petscan_images_download.py`
def gather_dataset(
    file_names: pd.DataFrame, total: int,
    project: str, output_dir: str,
    host: str = HOST, timeout: int = TIMEOUT,
) -> None:
    count, miss = 0, 0

    for file_name in tqdm(file_names.page_title, total=total):
        if count == total:
            break

        file_extension = file_name.split('.')[-1].lower()

        # Build the URL
        md5 = hashlib.md5(file_name.encode('utf-8')).hexdigest()
        shard = f'{md5[0]}/{md5[:2]}'
        width = compute_width(f'File:{file_name}')
        if project == 'commons':
            url = f'{host}/wikipedia/{project}/thumb/{shard}/{file_name}/{width}px-{file_name}'
        else:
            url = f'{host}/wikipedia/{project}/{shard}/{file_name}'
        # Handle rendered thumbnails
        if file_extension in ('djvu', 'djv', 'gif', 'tiff', 'tif', 'webp'):
            url += '.jpg'
            file_name += '.jpg'
        elif file_extension in ('svg', 'xcf'):
            url += '.png'
            file_name += '.png'

        # Fire the HTTP request
        try:
            res = requests.get(url, stream=True, verify=True, timeout=timeout)
        except Exception as e:
            print(f'{file_name} - endpoint not working: {e}')
            miss += 1
            continue

        if not res.ok:
            print(f'{file_name} - not OK: got HTTP {res.status_code}')
            miss += 1
            continue

        # Write file
        res_bytes = b''
        for chunk in res.iter_content(1024):
            res_bytes += chunk

        with open(os.path.join(output_dir, file_name), 'wb') as fout:
            fout.write(res_bytes)

        count += 1

    print()
    print('Missed files:', miss)
    print()


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description=(
            'Gather a dataset of images from a CSV of file names. '
            'The CSV should only contain image file names. Others will cause bad download '
            'requests and will be skipped.'
        )
    )
    parser.add_argument('input_csv', metavar='/path/to/csv', help='Input CSV file')
    parser.add_argument(
        '-o', '--output', metavar='/path/to/dir', default=OUTPUT_DIR,
        help=f'Output directory. Default: "{OUTPUT_DIR}"',
    )
    parser.add_argument(
        '-p', '--project', metavar='wiki', default=PROJECT,
        help=f'Wiki project to gather images from. Default: {PROJECT}'
    )
    parser.add_argument(
        '-t', '--total', type=int, metavar='N', default=TOTAL,
        help=f'How many images to download. Default: {TOTAL}'
    )

    return parser.parse_args()


def main():
    args = parse_args()
    input_csv = args.input_csv
    output_dir = args.output
    project = args.project
    total = args.total

    os.makedirs(output_dir, exist_ok=True)
    # https://wikitech.wikimedia.org/wiki/Data_Platform/Internal_API_requests#Python
    os.environ['REQUESTS_CA_BUNDLE'] = '/etc/ssl/certs/ca-certificates.crt'

    file_names = pd.read_csv(input_csv)
    gather_dataset(file_names, total, project, output_dir)

    return 0


if __name__ == '__main__':
    exit(main())
