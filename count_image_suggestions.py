#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from wmfdata.spark import create_session

from image_suggestions.queries import suggestions_with_feedback

SNAPSHOT = '2024-07-08'


def main():
    spark = create_session(app_name='isu-count')

    ddf = spark.sql(suggestions_with_feedback)
    counts = ddf.groupBy('wiki').count().toPandas()
    isu = spark.read.table('analytics_platform_eng.image_suggestions_suggestions').where(f"snapshot='{SNAPSHOT}'")
    alis = isu.where('section_index is null').groupBy('wiki').count().toPandas()
    slis = isu.where('section_index is not null').groupBy('wiki').count().toPandas()

    spark.stop()

    df = counts.merge(alis, on='wiki', suffixes=('_done', '_alis_total'))
    df = df.merge(slis, on='wiki')
    df = df.rename({'count_done': 'consumed', 'count_alis_total': 'alis_total', 'count': 'slis_total'}, axis=1)
    df.to_csv(f'~/misc_data/isu_counts_{SNAPSHOT}.csv', index=False)


if __name__ == '__main__':
    main()
