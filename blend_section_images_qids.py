#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from wmfdata.spark import get_session
from pyspark.sql import functions as F

spark = get_session(app_name='quids')

diego = spark.read.parquet('pt.parquet')
muniza = spark.read.parquet('section_images_2022-04.parquet')
img_qids = spark.read.table('analytics_platform_eng.image_suggestions_wikidata_data')
ptmuniz = muniza.where(muniza.wiki_db == 'ptwiki')
jo = diego.join(ptmuniz, [diego.page_wikidata_item == ptmuniz.item_id, diego.section_heading == ptmuniz.heading])
ptclean = jo.withColumnRenamed('page_wikidata_item', 'page_qid').withColumnRenamed('heading', 'section_title').withColumnRenamed('section_link_item_id', 'section_link_qid').withColumnRenamed('images', 'section_image_titles').select('page_id', 'page_qid', 'page_title', 'section_link_qid', 'section_title', 'section_image_titles')
commons_file_pages = """
SELECT page_id, page_title
FROM wmf_raw.mediawiki_page
WHERE page_namespace=6
AND page_is_redirect=0
AND wiki_db='commonswiki'
AND snapshot='2022-04'
"""
# NOPE can't join ptwiki IDs with commonswiki ones!
# ptimages = spark.sql("select page_id, page_title from wmf_raw.mediawiki_page where snapshot='2022-04' and wiki_db='ptwiki' and page_namespace=6 and page_is_redirect=0")
# TODO this is a re-compute: keep image titles on `analytics_platform_eng.image_suggestions_wikidata_data`
commons = spark.sql(commons_file_pages)
img_titles = img_qids.join(commons, img_qids.page_id == commons.page_id)
img_clean = img_titles.withColumnRenamed('item_id', 'image_qid').withColumnRenamed('page_title', 'image_title').select('image_qid', 'image_title')
ptexpl = ptclean.withColumn('section_image_titles', F.explode_outer(ptclean.section_image_titles))
ptnoficheiro = ptexpl.withColumn('section_image_titles', F.regexp_replace(ptexpl.section_image_titles, 'Ficheiro:', ''))
ptfinal = ptnoficheiro.join(img_clean, ptnoficheiro.section_image_titles == img_clean.image_title).withColumnRenamed('section_image_titles', 'section_image_title').withColumnRenamed('image_qid', 'section_image_qid').drop('image_title')
ptfinal.write.parquet('ptwiki_sections_images_qids', mode='overwrite')

spark.stop()

