#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Run me from my parent directory

import json
import requests
from collections import defaultdict

import mwparserfromhell as mwp
import pandas as pd
from pyspark.sql import functions as F
from wmfdata.spark import get_session

from section_topics import pipeline


RANDOM_SEED = 1611
N_SAMPLES = 1000

# api_url = 'https://{}.wikipedia.org/api/rest_v1/page/mobile-sections/{}/{}'
api_url = 'https://{}.wikipedia.org/w/api.php?action=parse&prop=sections&format=json&oldid={}'
headers = {'User-Agent': 'Marco Fossati, Structured Data team - mfossati@wikimedia.org'}

# with open('section_topics/wikipedias.txt') as win:
    # wikiz = [w.rstrip() for w in win]
wikiz = ('arwiki', 'bnwiki', 'cswiki', 'eswiki', 'idwiki', 'ptwiki', 'ruwiki')
spark = get_session(
    app_name='rest-api-bad-section-parsing-check', type='yarn-large',
    # app_name='action-api-bad-section-parsing-check', type='yarn-large',
    extra_settings={
        'spark.sql.shuffle.partitions': 2048,
        'spark.reducer.maxReqsInFlight': 1,
        'spark.shuffle.io.retryWait': '60s',
        'spark.driver.memory': '16g',
        'spark.driver.maxResultSize': '12g',
    }
)
wt = pipeline.load_wikitext(spark, '2022-09', ', '.join([f"'{w}'" for w in wikiz]))
wt.cache()

recap = defaultdict(dict)
full = defaultdict(list)

for w in wikiz:
    total, bad = 0, 0

    sample = wt.where(wt.wiki_db == w).where(wt.page_redirect_title == '').sample(1.0, seed=RANDOM_SEED).take(N_SAMPLES)

    for row in sample:
        p = mwp.parse(row.revision_text).get_sections(levels=[2], include_lead=True)
        parsed = len(p)

        try:
            # r = requests.get(api_url.format(w.rstrip('wiki'), row.page_title, row.revision_id), headers=headers)
            r = requests.get(api_url.format(w.rstrip('wiki'), row.revision_id), headers=headers)
        except:
            print('API not ok, skipping:', row.wiki_db, row.page_title, row.revision_id)
            continue

        if not r.ok:
            print('Response not ok, skipping:', r.status_code, row.wiki_db, row.page_title, row.revision_id)
            continue

        try:
            j = r.json()
        except:
            print('No JSON, skipping:', row.wiki_db, row.page_title, row.revision_id)
            continue

        # api, no_level = 0, 0
        api, no_level = 1, 0  # Assumes lead section everywhere

        # lead = j.get('lead')
        # if lead is not None:
            # api += 1
        # else:
            # print('No lead section:', row.wiki_db, row.page_title, row.revision_id)

        try:
           # others = j['remaining']['sections']
           others = j['parse']['sections']
           for s in others:
               level = s.get('toclevel')
               if level is None:
                   print('Section with no level, skipping')  # NOTE this might bias the report
                   no_level += 1
                   continue
               if level == 1:
                   api += 1
        except KeyError:
            print('No other sections:', row.wiki_db, row.page_title, row.revision_id)

        if parsed != api:
            bad += 1

        total += 1

        full[w].append(
            {'page_title': row.page_title, 'revision_id': row.revision_id, 'mwp': parsed, 'api': api, 'no_level': no_level}
        )

    recap[w]['total'] = total
    recap[w]['bad'] = bad
    if total != 0 and bad != 0:
        recap[w]['ratio'] = bad/total

spark.stop()

df = pd.DataFrame.from_dict(recap, orient='index')
ratio = df.ratio.describe().to_string()

with open('/home/mfossati/samples/action_api_bad_parsing_report_target_wikis.json', 'w') as jout:
    json.dump(recap, jout, indent=2)
with open('/home/mfossati/samples/action_api_bad_parsing_full_report_target_wikis.json', 'w') as jout:
    json.dump(full, jout, indent=2, ensure_ascii=False)
with open('/home/mfossati/samples/action_api_bad_parsing_target_wikis.txt', 'w') as fout:
    fout.write(ratio)
