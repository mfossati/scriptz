#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Filter grayscale images given their average saturation.
# This script was used to curate a training set for image classification.
# See https://phabricator.wikimedia.org/T370861.

# Inputs available in stat1010.
# `convert` command from https://superuser.com/a/508476.

from os import copy
import pandas as pd

# Signature
# find ~/images/svg_signature ~/images/pd_signature -type f -printf '%h/%f\t' -exec convert "{}" -colorspace HSL -channel g -separate +channel -format "%[fx:mean]\n" info: \; > ~/signatures_avg_saturations.tsv
df = pd.read_csv('signatures_avg_saturations.tsv', names=('img', 'sat'), sep='\t')
df.sat.describe(percentiles=(.25, .5, .75, .9))
good = df[df.sat <= 0.310765]
for row in good.itertuples(index=False):
    copy(row.img, '/home/mfossati/training/signature/')

# Chemical structure
# find ~/images/svg_chemical_structure -type f -printf '%h/%f\t' -exec convert "{}" -colorspace HSL -channel g -separate +channel -format "%[fx:mean]\n" info: \; > ~/svg_chemical_structures_avg_saturations.tsv
df = pd.read_csv('svg_chemical_structures_avg_saturations.tsv', names=('img', 'sat'), sep='\t')
clean = df[~df.img.str.contains(r'logo|icon|blason|flag|diagram|coat|emblem|escudo', case=False)]
clean.sat.describe(percentiles=(.25, .5, .75, .9))
good = clean[clean.sat < 0.2]
for row in good.itertuples(index=False):
    copy(row.img, '/home/mfossati/training/chemical_structure/')

# Glyph
# find ~/images/svg_glyph -type f -printf '%h/%f\t' -exec convert "{}" -colorspace HSL -channel g -separate +channel -format "%[fx:mean]\n" info: \; > ~/svg_glyph_avg_saturations.tsv
df = pd.read_csv('svg_glyph_avg_saturations.tsv', names=('img', 'sat'), sep='\t')
df.sat.describe(percentiles=(.25, .5, .75, .9, .95))
good = df[df.sat <= 0.133366]
for row in good.itertuples(index=False):
    copy(row.img, '/home/mfossati/training/glyph/')
